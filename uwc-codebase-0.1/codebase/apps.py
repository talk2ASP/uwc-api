from __future__ import unicode_literals

from uwc.apps import AppConfig


class CodebaseConfig(AppConfig):
    name = 'codebase'
