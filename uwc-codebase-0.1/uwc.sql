-- --------------------------------------------------------
-- Host:                         ycea-db.cbxs7vbzf7tw.ap-south-1.rds.amazonaws.com
-- Server version:               8.0.23 - Source distribution
-- Server OS:                    Linux
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for uwc-staging
CREATE DATABASE IF NOT EXISTS `uwc-staging` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `uwc-staging`;

-- Dumping structure for table uwc-staging.uwc_banner
CREATE TABLE IF NOT EXISTS `uwc_banner` (
  `id` int NOT NULL AUTO_INCREMENT,
  `banner_url` text,
  `status` binary(50) DEFAULT '1\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `priority` smallint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Data exporting was unselected.

-- Dumping structure for table uwc-staging.uwc_determined_list
CREATE TABLE IF NOT EXISTS `uwc_determined_list` (
  `id` int NOT NULL AUTO_INCREMENT,
  `task_id` int NOT NULL,
  `address_phno` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `status` enum('OPEN','COMPLETE') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'OPEN',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table uwc-staging.uwc_form
CREATE TABLE IF NOT EXISTS `uwc_form` (
  `id` int NOT NULL AUTO_INCREMENT,
  `form_title` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `form_description` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `form_html` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `created_by` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table uwc-staging.uwc_form_question_mapping
CREATE TABLE IF NOT EXISTS `uwc_form_question_mapping` (
  `id` int NOT NULL AUTO_INCREMENT,
  `form_id` int NOT NULL,
  `ques_id` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qts_text` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ans_type` enum('TX','NM','DT','DD') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `min_length` int NOT NULL DEFAULT '1',
  `max_length` int DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ch1` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ch2` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ch3` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ch4` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ch5` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ch6` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ch7` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ch8` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ch9` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ch10` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table uwc-staging.uwc_project
CREATE TABLE IF NOT EXISTS `uwc_project` (
  `id` int NOT NULL AUTO_INCREMENT,
  `project_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_description` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `team_id` int DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_campaign` bit(1) NOT NULL DEFAULT b'0',
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('OPEN','COMPLETE') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'OPEN',
  `project_start_date` date DEFAULT NULL,
  `project_end_date` date DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `is_project_active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table uwc-staging.uwc_project_task_mapping
CREATE TABLE IF NOT EXISTS `uwc_project_task_mapping` (
  `id` int NOT NULL AUTO_INCREMENT,
  `project_id` int NOT NULL,
  `task_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `task_status` enum('OPEN','COMPLETE') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'OPEN',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `completed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `task_type` enum('SIMPLE','FORM') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `form_id` int DEFAULT NULL,
  `team_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `task_instructions` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `has_determined_list` tinyint(1) NOT NULL DEFAULT '0',
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table uwc-staging.uwc_role
CREATE TABLE IF NOT EXISTS `uwc_role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `role_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `role_description` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_na_priority` int DEFAULT NULL,
  `module_access` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `report_access` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table uwc-staging.uwc_task_survey
CREATE TABLE IF NOT EXISTS `uwc_task_survey` (
  `id` int NOT NULL AUTO_INCREMENT,
  `form_id` int DEFAULT NULL,
  `project_id` int DEFAULT NULL,
  `task_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `survey_number` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `address1` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `address2` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `pincode` int DEFAULT NULL,
  `state_code` char(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `pic_url` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('OPEN','COMPLETE') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'OPEN',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `a1` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a2` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a3` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a4` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a5` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a6` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a7` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a8` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a9` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a10` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a11` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a12` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a13` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a14` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a15` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a16` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a17` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a18` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a19` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a20` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a21` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a22` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a23` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a24` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a25` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table uwc-staging.uwc_team
CREATE TABLE IF NOT EXISTS `uwc_team` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `team_description` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `team_title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_campaign_team` tinyint(1) DEFAULT '0',
  `is_team_active` tinyint(1) NOT NULL DEFAULT '1',
  `team_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'Functional',
  `team_pic_url` varchar(600) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'https://s3.ap-south-1.amazonaws.com/uwc.data/user/profile/default.png',
  `created_by` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table uwc-staging.uwc_team_user_mapping
CREATE TABLE IF NOT EXISTS `uwc_team_user_mapping` (
  `id` int NOT NULL AUTO_INCREMENT,
  `team_id` int NOT NULL,
  `user_id` int NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `team_id_user_id` (`team_id`,`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table uwc-staging.uwc_user
CREATE TABLE IF NOT EXISTS `uwc_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` bigint NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_id` int DEFAULT NULL,
  `state_code` char(5) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `district_code` int DEFAULT NULL,
  `assembly_code` int DEFAULT NULL,
  `block_code` int DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` char(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ACTIVE',
  `profile_pic_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'https://s3.ap-south-1.amazonaws.com/uwc.data/user/profile/default.png',
  `date_of_birth` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `activity_points` int DEFAULT NULL,
  `facebook` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `telegram` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mobile` (`mobile`)
) ENGINE=InnoDB AUTO_INCREMENT=1112 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table uwc-staging.uwc_user_session
CREATE TABLE IF NOT EXISTS `uwc_user_session` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `token` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ACTIVE',
  `device_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table uwc-staging.uwc_user_verification
CREATE TABLE IF NOT EXISTS `uwc_user_verification` (
  `id` int NOT NULL AUTO_INCREMENT,
  `mobile` bigint NOT NULL,
  `otp` int NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mobile` (`mobile`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
